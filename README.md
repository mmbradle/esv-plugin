# ESV Plugin for Anki

## Hello and Welcome!
The entire point of this plugin is to aid you in the memorization of scripture. I am terrible at memorizing scripture (worse than you even) and I have memorized hundreds of verses using this method. 

Try it. You will probably love it.

Unfortuneately, it is a little hard to setup Anki to use for scripture memory. That's why I created this plugin: to make it easier. Hopefully you'll find the process straight forward with the instructions included below.

God bless,

Mike
 
## Directions

### Install and Run Anki
1. There are instructions at [Anki's website](http://ankisrs.net/) which will help you download and install Anki.

### Install "Esv Scripture Memory" Addon

1. In Anki, select: `Tools -> Add-ons -> Browse & Install...`
2. In the Insall Add-on dialog, enter `1225456658` and hit `Ok`
3. Anki will install the add-on and prompt you for a restart which is necessary.
4. Restart Anki

### Setup Options (Basic)
At this point, you may be in a fresh installation of Anki with a fresh user, or you might be adding this plugin to an existing installation of Anki. Either way the steps are the same.

1. First we need to create an Anki *Note Type* and several *Card Types* for all your new scripture *Notes* and *Cards*. Think of the *Note Type* and *Card Types* as templates that contains all the *fields* and styling necessary to store the information that you want to be seen during your reviews. Don't worry, this plugin will create all this for you. For a further explanation of these terms, please refer to the [Anki documentation](http://ankisrs.net/docs/manual.html#the-basics).
2. In Anki, select: *Tools -> Esv-Plugin Options*
3. Pick a name for your *Note Type*. I suggest leaving the default which is *ESV Verse*
**Warning**, *The next step is semi-permanent*: 
4. Click `Create`. This will create the template information needed to create cards. 
    1. Once you click create, you will not be able to change your mind later easily. At least not until the plugin author decides to implement code to support it.
5. I would suggest turning on the `Use Improved Spell Checking`
    1. This option will make the spell check work in a per-word way instead of a per-character way. That is, the spell checker will show you words you missed instead of showing you characters you missed. This is more helpful than the default Anki behavior.
6. For the `TTS Engine`, chose `Google TTS` 
    1. The Ivona option will sound the best. This TTS Engine setting is for creating a computer generated voice to read the verse references only. All scripture text will be read by a professional speaker that Crossway makes available on their website. The Ivona option does require some additional setup, which I will describe below.

### Setup Options (Advanced)

#### Setup custom decks
Will complete these instructions later

#### Setup custom intervals
Will complete these instructions later

#### Setup Ivona
1. First you will need a couple of secret key phrases. There are two ways to get these:
    1. If you know my email address, send me an email and I'll let you use my key phrases.
    2. If you don't know me or my email address and you still want to setup Ivona (which I recommend), then you will have to setup a developer account at [Ivona](https://www.ivona.com/us/account/speechcloud/status).
2. Select `Ivona` in the Esv-Plugin Options and two new text fields will open up. Enter the shorter number in the first field and the longer number in the second field.
3. Hit `Ok`

### Add Verses to Memorize
Now we're getting to the fun stuff!

1. In the main page of Anki (Decks) click `Add`
2. For `Type` make sure `ESV Verse` is selected (or whatever you set for your `Note Type` above).
3. Select whatever deck you would like to use for you memory verses. Any deck will work, this is an organizational decision on your part.
4. Add a verse reference such as James 3:18.
* Jam 3:18 will also work. Lots of things work, this add-on uses Crossway's API to get memory verses and they support entering verses lots of different ways. Try your favorite and see if it works.
5. You see the button bar that begins with a big ***B***? Way over on the right side of this bar of buttons is a down arrow. Click this down arrow and then click `get verse`.
* If `get verse` does not show up for you, make sure you are on the `Type` that is setup in the options dialog.
6. Hopefully a bunch of information just got set for you. Text was downloaded; audio was downloaded; and you didn't have to do anything. You could be looking at an error right now. If that's the case, I'm sorry, I'll do my best to get this thing error free as time goes on.
7. Further customize your card. I'll explain how you might want to do this in *Customize Your Verse* below.
7. Click `Add` and away you go.

#### Customize Your Verse
Here are my suggestions on how to setup your verse for learning.
##### Hint
Think of `Hint` as a title. This can often be the first few words of the verse. For John 3:16 I have "For God so Loved" as my hint. For other verses I put a key phrase from the verse as the hint. For 2 Timothy 3:16 I have "God Breathed" as my hint. If you don't set your own hint, the add-on will add the first few words of the verse for you. Feel free to change the text to whatever is most useful for you.

Also, when Anki asks you to recite a verse like James 3:18, you might just need a little help to remember what James 3:18 is about. Clicking `Show Hint` at this point will show you whatever you have set in this field. See why I called it hint?

#### Image
I set an image about half of the time. If I am having a hard time with a particular verse I'll add an image to help me remember it. Sometimes I add an image just because I know of the perfect image. I suggest using Google images for this and then using a screen capture utility to grab images and pasting them into this field.

#### Notes
I add a notes field to almost all of my Anki cards. I might add some personal information about this verse or add some contextual information. Use this field for whatever you want, this text wont show up during your reviews.

#### Create Recognize Card / Create Recite Card
This is important. 

Anki will generate at most 3 flash cards for every verse you add: *recite*, *recognize*, and *lookup*. 

**Recite** is where you have to word-for-word remember the verse. **Lookup** is where you have to remember the reference for the verse. **Recognize** is where you have to remember what the verse is about without having to recite it word for word. Recognize and lookup should be *fast* recite is *slow*.

If you put any text in the `Create Recognize Card?` field, then Anki will create a recognize card for you. Similarly, if you put any text in the `Create Recite Card?` field, then Anki will create a recite card for you. The default is a 'y' in recognize and recite is left blank.

You, like most people, probably think that the recite part is the most important part of memorization. I used to think that way to. With time, you will probably find this to be less of the case. By all means, start by adding a 'y' in both boxes. Anki will make you memorize the verse word-for-word, which is great to start with. With time, you'll want to add more verses quickly and you'll find your reviews slowing down. Try adding verses without the recite option. You'll memorize more verses, your reviews will go more quickly and you will probably find yourself memorizing the verses you care about word-for-word anyway. It kinda happens by osmosis. You'll see! It's great!

### Reviewing
1. So now you have some cards to learn.
2. My first piece of advice is this: don't learn to much to quickly. You're excited to learn and memorize verses. You add one verse, start the review process, and fifteen seconds later you're done. Take my advice, stop for the day. Maybe add one or two more verses, but after that, really, stop. Its boring I realize. You're all jazzed up about all the work you've put into this and you want to *work!* The work of Anki happens over days and weeks, not in minutes and hours. Let it go. Come back to it tomorrow.
3. You have to decide for yourself what is "good enough" to count as a pass. If I miss one word is that a pass or a fail? If I miss a reference by one number is that a pass or a fail. If I remember everything correctly but it takes me quite a long time and a lot of effort should that be a pass or a fail. In making these decisions remember a couple of things
    1. Anki is trying to schedule reviews as far apart as possible. Whatever your standard for saying pass/fail is, you will tend to learn a card to that degree of speed and accuracy. Do you want to be able to recall this verse in less than a second, than anything less than a second should be a failure. Do you want to be able to find a passage in the Bible when you go to look it up, than that should be your standard.
    2. After you fail and Anki resets the intervals of the card, it is really fast to bring it back up to where you were, only a few seconds of total review time will get you back to an interval of over a month.  

### General Information

#### What Verses to Add?
If you are looking for suggestions, here are a couple web pages that list popular verses.

1. [Biblegateway's 100 Most Read Verses](https://www.biblegateway.com/blog/2009/05/the-100-most-read-bible-verses-at-biblegatewaycom/)
2. [TopVerses.com](http://www.topverses.com/)
3. [Navigator's Topical Memory Verses](https://www.navigators.org/Tools/Discipleship%20Resources/Tools/Topical%20Memory%20System)

#### Why a Spaced Repetition System (SRS) such as Anki?
I'll add more information here later.