# @PydevCodeAnalysisIgnore
# pylint: skip-file

from logging import Logger
import logging
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from anki.hooks import wrap
from aqt.editor import Editor
from anki.utils import namedtmp
from aqt import mw
from aqt.reviewer import *
from aqt.utils import showInfo
from esv_plugin.esvNoteType import populate_note
from esv_plugin.spell.spellCheck import checkSpelling
from esv_plugin.ui.esvOptionsWidget import EsvOptions, EsvOptionsWidget


# Next 2 lines will allow children loggers to propagate up to this config without letting this config propagate up to Anki
LOG = logging.getLogger('esv_plugin')
LOG.propagate = False

LOG.setLevel(logging.DEBUG)
file_handler = logging.FileHandler(os.path.join(mw.pm.addonFolder(), 'esv_plugin', 'anki_esv_plugin.log'))
file_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s [in: %(name)s.%(funcName)s()]')
file_handler.setFormatter(formatter)
LOG.addHandler(file_handler)
LOG.info('--> LOG STARTED <--')


# Create functionality to populate ESV Verse Notes
def do_populate_note(editor):
    populate_note(editor, mw.esvOptions, namedtmp)


# Update menu
def onAdvancedReplacement(self, _old):
    '''
    This function replaces Editor.onAdvanced()

    All but the last couple of lines are lifted from onAdvance().
    This could be handled better
    '''
    try:
        LOG.info("Augmenting Advanced toolbar with ESV functionality for note type '%s'.", mw.esvOptions.noteType)
        LOG.debug("self: %s, type: %s, class: %s", self, type(self).__name__, self.__class__.__name__)
        LOG.debug("Current note type: '%s'.", self.note.model()['name'])
        m = QMenu(self.mw)
        a = m.addAction(_("LaTeX"))
        a.setShortcut(QKeySequence("Ctrl+t, t"))
        a.connect(a, SIGNAL("triggered()"), self.insertLatex)
        a = m.addAction(_("LaTeX equation"))
        a.setShortcut(QKeySequence("Ctrl+t, e"))
        a.connect(a, SIGNAL("triggered()"), self.insertLatexEqn)
        a = m.addAction(_("LaTeX math env."))
        a.setShortcut(QKeySequence("Ctrl+t, m"))
        a.connect(a, SIGNAL("triggered()"), self.insertLatexMathEnv)
        a = m.addAction(_("Edit HTML"))
        a.setShortcut(QKeySequence("Ctrl+shift+x"))
        a.connect(a, SIGNAL("triggered()"), self.onHtmlEdit)
        if self.note.model()[u"name"].upper() == mw.esvOptions.noteType.upper():
            LOG.debug('Adding extra ESV menu item.')
            a = m.addAction(_("Get verse"))
            a.setShortcut(QKeySequence("Ctrl+g, g"))
            a.connect(a, SIGNAL("triggered()"), lambda self=self: do_populate_note(self))
        m.exec_(QCursor.pos())
    except StandardError:
        LOG.exception("Error caught while updating GUI for plugin")
        raise
# Replace function
Editor.onAdvanced = wrap(Editor.onAdvanced, onAdvancedReplacement, "around")



# def setupButtons(self):
#     LOG.info("Augmenting Standard toolbar with ESV functionality for note type '%s'.", mw.esvOptions.noteType)
#     LOG.debug("self: %s, type: %s, class: %s", self, type(self).__name__, self.__class__.__name__)
#     try:
#         if self.note.model()[u"name"].upper() == mw.esvOptions.noteType.upper():
#             self._addButton(
#                 "verseButton", lambda self=self: populate_note(self, mw.esvOptions, namedtmp),
#                 text=u"In", tip="Lookup Verse (Ctrl+Shift+V)", key="Ctrl+Shift+V")
#     except StandardError:
#         LOG.exception("Error caught while augmenting Standard toolbar.")
#         raise
# Editor.setupButtons = wrap(Editor.setupButtons, setupButtons)


# Setup override for correct
correct_orig = mw.reviewer.correct  # @UndefinedVariable
def correct_new(given, correct, showBad=False):
    try:
        LOG.debug('Performing tolerant correction with given: "%s", correct: "%s", showBad "%s"')
        if mw.esvOptions.useSpell == True:
            return checkSpelling(given, correct)
        else:
            return correct_orig(given, correct, showBad)
    except StandardError:
        LOG.exception("Error caught during review comparison.")
        raise
mw.reviewer.correct = correct_new


# Register Options in anki
def onEsvOptions():
    try:
        LOG.info("Showing ESV Options Widget.")
        mw.esvOptionsWidget = EsvOptionsWidget(mw.esvOptions, mw.col.models)
        mw.esvOptionsWidget.show()
    except StandardError:
        LOG.exception("Unexpected error while displaying ESV Options Widget.")
        raise

def registerEsvOptionsWidget():
    try:
        LOG.info("Registering ESV Options Widget")
        options_action = QAction("Esv-Plugin Options", mw)
        mw.connect(options_action,
                SIGNAL("triggered()"),
                onEsvOptions)
        mw.form.menuTools.addAction(options_action)  # @UndefinedVariable
    except StandardError:
        LOG.exception("Unexpected error while registering ESV Options Widget.")
        raise
registerEsvOptionsWidget()

# Save options
def onProfileLoaded():
    try:
        LOG.info("Loading ESV Options from disk")
        mw.esvOptions = EsvOptions()
        mw.esvOptions.loadPluginData(mw.pm.profileFolder())  # @UndefinedVariable
    except StandardError:
        LOG.exception("Unexpected error while loading ESV Options")
        raise
addHook("profileLoaded", onProfileLoaded)

# Load options
def onProfileUnloaded():
    try:
        LOG.info("Saving EsvOptions to disk")
        mw.esvOptions.savePluginData(mw.pm.profileFolder())  # @UndefinedVariable
    except StandardError:
        LOG.exception("Unexpected error while saving ESV Options")
        raise
addHook('unloadProfile', onProfileUnloaded)

