import re


PAUSE_TEXT = ','

'''
Simple regex to handle references.
Does not meet more complicated references such as a range that spans a chapter
'''
referenceRegex = re.compile(r"""
 \b                             # word boundary (not strictly necessary)
 (?P<book>(\d\s)?\w+)           # book
 \s*                            # white space trailing book name
 ((?P<chapter>\d{1,3})          # chapter
 :)?                            # colon
 (?P<verse>\d{1,3}(-\d{1,3})?)  # verse
""", re.VERBOSE)


class ScriptureReference (object):
    def __init__(self, book, chapter, verse):
        self.book = book
        self.chapter = chapter
        self.verse = verse

    @classmethod
    def parseRefText(cls, text):
        match = referenceRegex.match(text)
        if match:
            bookText = match.group('book')
            chapterText = match.group('chapter')
            verseText = match.group('verse')
            return ScriptureReference(bookText, chapterText, verseText)

    def getRefForTts(self, substitution=None):
        newBookName = self.book
        if substitution:
            newBookName = substitution.getPronounciation(self.book)

        pauseText = ""
        verseText = self.verse
        if '-' in verseText:
            pauseText = PAUSE_TEXT
            verseText = verseText.replace('-', ' to ')

        text = "%s %s%s %s" % (newBookName, self.chapter, pauseText, verseText)
        return text

    def getRefAsFile(self, ext='', suffix=''):
        if suffix:
            suffix = "_" + suffix
        if ext:
            ext = "." + ext

        newChapter = ""
        if self.chapter:
            newChapter = "_" + self.chapter
        newVerse = ""
        if self.verse:
            newVerse = "_" + self.verse

        text = "%s%s%s%s%s" % (self.book, newChapter, newVerse, suffix, ext)
        return text.replace(" ", "")


class Verse (object):
    def __init__(self, reference, text, mp3Link):
        self.reference = reference
        self.text = text
        self.mp3Link = mp3Link
