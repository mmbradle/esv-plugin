from HTMLParser import HTMLParser
import logging
import urllib


LOG = logging.getLogger(__name__)


class AudioFormat (object):
    FLASH = 'flash'
    MP3 = 'mp3'
    REAL = 'real'
    WMA = 'wma'


class AudioVersion (object):
    HW = 'hw'  # David Cochran Heath [Hear the Word], complete Bible
    MM = 'mm'  # Max McLean, complete Bible,
    ML = 'ml'  # Marquis Laughlin, New Testament only,
    ML_MM = 'ml-mm'  # Max McLean for Old Testament, Marquis Laughlin for New Testament


class EsvSession:
    def __init__(self, key='IP'):

        self.__BASE_URL = 'http://www.esvapi.org/v2/rest/passageQuery'

        self.key = key

        self.plainTextDict = {
               'output-format': 'plain-text',
               'include-passage-references': 'true',
               'include-first-verse-numbers': 'false',
               'include-verse-numbers': 'false',
               'include-footnotes': 'false',
               'include-short-copyright': 'false',
               'include-copyright': 'false',
               'include-passage-horizontal-lines': 'false',
               'include-heading-horizontal-lines': 'false',
               'include-headings': 'false',
               'include-subheadings': 'false',
               'include-selahs': 'false',
               'include-content-type': 'false',
               'line-length': '74'
               }

        self.xmlDict = {
                'output-format': 'crossway-xml-1.0',
                'include-doctype': 'false',
                'include-quote-entities': 'false',
                'include-simple-entities': 'false',
                'include-cross-references': 'false',
                'include-line-breaks': 'false',
                'include-word-ids': 'false',
                'include-virtual-attributes': 'false',
                'base-element': 'paragraph'  # paragraph or verse-unit
                }

        self.htmlDict = {
                'output-format': 'html',
                'include-passage-references': 'true',
                'include-verse-numbers': 'false',
                'include-footnotes': 'false',
                'include-footnote-links': 'false',
                'include-headings': 'false',
                'include-subheadings': 'false',
                'include-surrounding-chapters': 'false',
                'include-word-ids': 'false',
#                 'link-url': 'http://www.gnpcb.org/esv/search/',
                'include-audio-link': 'false',
                'audio-format': AudioFormat.MP3,
                'audio-version': AudioVersion.HW,
                'include-short-copyright': 'false',
                'incldddude-copyright': 'false'
                }

    def getVerseContents(self, ref):
        result = self.__doQuery(ref, self.plainTextDict, updateParams={'include-passage-references': 'false'})
        result = unicode(result)
        result = " ".join(result.split())
        return result

    def getFullReference(self, ref):
        result = self.__doQuery(ref, self.plainTextDict, {})
        return result.split('\n', 1)[0]

    def getMp3Link(self, ref, audioVersion=AudioVersion.HW):
        updateParams = {
                'include-audio-link': 'true',
                'audio-version': audioVersion
                 }
        result = self.__doQuery(ref, self.htmlDict, updateParams)

        return EsvAudioHTMLParser().getUrl(result)

    def __doQuery(self, ref, baseParams, updateParams):
        queryParams = {'passage': ref, 'key': self.key}
        queryParams.update(baseParams)
        queryParams.update(updateParams)
        queryString = urllib.urlencode(queryParams)
        url = "%s?%s" % (self.__BASE_URL, queryString)
        LOG.debug("url: %s", url)
        response = urllib.urlopen(url)
        return response.read()


class EsvAudioHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.inAudio = False
        self.audioUrl = ""

    def handle_starttag(self, tag, attrs):
        # handle <small class="audio">
        if tag == "small":
            for name, value in attrs:
                if name == 'class' and value == 'audio':
                    self.inAudio = True
        if tag == 'a':
            if self.inAudio:
                for name, value in attrs:
                    if name == 'href':
                        self.audioUrl = value

    def handle_endtag(self, tag):
        if tag == "small":
            self.inAudio = False

    def getUrl(self, data):
        self.feed(data)
        return self.audioUrl
