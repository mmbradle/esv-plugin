'''
This module is used for replacing the default answer comparison functionality of Anki.
The main difference is that this module checks the spelling of each word by itself and without punctuation.
'''

import difflib
import logging
import string
import unicodedata 


LOG = logging.getLogger(__name__)


def checkSpelling(given, correct):
    LOG.debug('given:\n"%s"\ncorrect:\n"%s"', given, correct)

    return SpellCheck().spell_check(given, correct)

# mw.reviewer.correct = checkSpelling


def longest_common_substring(string1, string2):
    '''
    Finds the longest common substring between string1 and string2
    '''
    # build matrix
    matrix = [[0] * (1 + len(string2)) for _ in xrange(1 + len(string1))]
    longest, x_longest = 0, 0
    for x_coord in xrange(1, 1 + len(string1)):
        for y_coord in xrange(1, 1 + len(string2)):
            if string1[x_coord - 1] == string2[y_coord - 1]:
                matrix[x_coord][y_coord] = matrix[x_coord - 1][y_coord - 1] + 1
                if matrix[x_coord][y_coord] > longest:
                    longest = matrix[x_coord][y_coord]
                    x_longest = x_coord
            else:
                matrix[x_coord][y_coord] = 0
    return string1[x_longest - longest: x_longest]


def find_substring(substring, searched_string):
    '''
    Finds position of substring in searched_string
    :param substring: the substring to find
    :param searched_string: the searched_string to find the substring in
    :returns: the position of the substring in searched_string
    '''
    done = False
    i = 0
    for i in range(len(searched_string)):
        for j in range(len(substring)):
            if searched_string[0 + i + j] == substring[j]:
                done = True  # passes so far, loop again
            else:
                done = False
                break
        if done == True:
            break
    if done:
        return i
    else:
        return -1


class SpellCheck(object):
    '''
    A class that is used for creating answer comparisons
    '''


    def __init__(self):
        self.anki_mw = None

    ok_color = "#00FF00"
    replace_color = "#FF0000"
    delete_color = "#FF0000"
    insert_color = "#FFFF00"
    
    EQUAL_TAG = "equal"
    REPLACE_TAG = "replace"
    DELETE_TAG = "delete"
    INSERT_TAG = "insert"
    font_size = "12"


    def __writeWords(self, given_words_immutable, i1, i2=None):
        retVal = "<span style='font-size: %spx; text-decoration: underline;'>" % (self.font_size)
        if i2:
            retVal += ' '.join(given_words_immutable[i1:i2])
        else:
            retVal += given_words_immutable[i1]
        retVal += "</span>"
        return retVal

    def spell_check(self, given, correct):
        '''
        Checks correct_words given string against correct_words correct string for correctness,
        and returns an html fragment to help the user visualize the differences.
        :param given: The text the user thinks is correct
        :param correct: The actually correct text to spell_check_word_based against
        :return: The html fragment to show the user
        '''

        # Do nothing. User probably accidently entered a character or two.
        if len(given) < 4 :
            return ""

        # Split both strings by spaces into arrays of words
        correct_words = correct.split()
        given_words = given.split()

        # We're going to modify the previous two arrays, keep these around unmodified for later
        correct_words_immutable = list(correct_words)
        given_words_immutable = list(given_words)

        # Strip punctuation and captialization, we want to check spelling, not dot and tittles
        for i in range(len(correct_words)):
            correct_words[i] = self.__strip_punctuation(correct_words[i])
            correct_words[i] = correct_words[i].upper()
        for i in range(len(given_words)):
            given_words[i] = self.__strip_punctuation(given_words[i])
            given_words[i] = given_words[i].upper()

        # Difflib does the heavy lifting. Also we dealt with junk ourselves, so pass None for that
        sequenceMatcher = difflib.SequenceMatcher(isjunk=None, a=given_words, b=correct_words)

        # We're going to use the font and size from Anki, maybe it would be better to style it ourselves though?
        try:
            self.font_size = self.anki_mw.bodyView.main.currentCard.cardModel.answerFontSize
            font_family = self.anki_mw.bodyView.main.currentCard.cardModel.answerFontFamily
        except AttributeError:
            # Couldn't get Anki styling. Revert to doing it ourselves
            self.font_size = 18
            font_family = "Times New Roman"

#         ok_style_string = ""
#         replace_style_string = "display: inline-block; position: relative; top: -1em; left: 0em;   font-size: .8em; width: 0"
#         delete_style_string = "color: red; text-decoration: line-through;"
#         insert_style_string = "color: red; text-decoration: underline;"

        # The snippet of HTML code to display the spelling results
        html_fragment = "<div style='font-size: %spx; font-family: %s; text-align: left;'>" % (self.font_size * .9, font_family)
        html_fragment += "Mistakes: <ol margin: .2em;'>"
        
        hasMistakes = False

        # opcodes
        #      a list of (tag, i1, i2, j1, j2) tuples, where tag is
        #      one of
        #          'replace'   a[i1:i2] should be replaced by b[j1:j2]
        #          'delete'    a[i1:i2] should be deleted
        #          'insert'    b[j1:j2] should be inserted
        #          'equal'     a[i1:i2] == b[j1:j2]
        for tag, i1, i2, j1, j2 in sequenceMatcher.get_opcodes():
            if tag == self.EQUAL_TAG:
                pass
#                 html_fragment += self.__create_span('okTxt', ok_style_string, " ".join(correct_words_immutable[j1:j2]))
            elif tag == self.REPLACE_TAG:
                hasMistakes = True
                html_fragment += "<li style='margin-bottom: .2em;'>"
                html_fragment += 'You wrote: ' + self.__writeWords(given_words_immutable, i1, i2)+ '<br>'
                html_fragment += 'Replacing: ' + self.__writeWords(correct_words_immutable, j1, j2)
                html_fragment += ' in: ' + self.__writeWords(correct_words_immutable, j1-1, j2+1)
                html_fragment += "</li>"
            elif tag == self.DELETE_TAG:
                hasMistakes = True
                html_fragment += "<li>"
                html_fragment += 'You added: ' + self.__writeWords(given_words_immutable, i1, i2)
                html_fragment += ' between: ' + self.__writeWords(correct_words_immutable, j1-1) 
                html_fragment += ' and: ' + self.__writeWords(correct_words_immutable, j2)
                html_fragment += "</li>"
            elif tag == self.INSERT_TAG:
                hasMistakes = True
                html_fragment += "<li>"
                html_fragment += 'You missed: ' + self.__writeWords(correct_words_immutable, j1, j2)
                html_fragment += ' in: ' + self.__writeWords(correct_words_immutable, j1-1, j2+1)
                html_fragment += "</li>"
        html_fragment += "</ol>"
        html_fragment += '</div>'
        
        if hasMistakes:
            LOG.debug(html_fragment)
            return html_fragment
        return "Correct!"
    
    def __create_span(self, classText, styleText, text):
        span_text = "<span" ;
        if classText: span_text += " class='%s'" % (classText)
        if styleText: span_text += " style='%s'" % (styleText)
        span_text += ">" ;
        span_text += text;
        span_text += "</span>" ;
        span_text += "\n" ;
        return span_text

    def __strip_accents(self, input_string):
        '''
        Removing punctuation does not remove accented characters, this should
        :param input_string: The string from which to remove accents
        :return: The input string without accents
        '''
        return unicodedata.normalize('NFKD', input_string).encode('ASCII', errors='ignore')

    def __strip_punctuation(self, input_string):
        '''
        Removes punctuation from string
        :param input_string: The string from which to remove punctuation
        :return: The input string without punctuation
        '''
        input_string = self.__strip_accents(input_string)
        input_string = input_string.translate(None, string.punctuation)
        return input_string
