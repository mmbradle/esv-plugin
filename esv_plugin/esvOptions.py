
import logging
import os
import pickle

LOG = logging.getLogger(__name__)

DATA_FILE_NAME = "esv_options.dat"
MEDIA_FOLDER_NAME = "collection.media"


class EsvOptions(object):
    def __init__(self):
        self.version = "0.1"
        self.noteType = "ESV Verse"
        self.useSpell = True
        self.useGoogleTts = False
        self.useVoiceRssTts = False
        self.useIvonaTts = False
        self.accessKey = ""
        self.secretKey = ""

    def __updateFromDict(self, **entries):
        '''
        Populates internal class fields based on a dictionary.
        I do not know how this works it is a hack I found online.
        The signature is strange and so it is delegated by updateFromDict.
        '''
        self.__dict__.update(entries)

    def updateFromDict(self, dictionary):
        ''' Populates internal class fields based on a dictionary '''
        self.__updateFromDict(**dictionary)  # pylint: disable=star-args

    def update(self, noteType, useSpell, useGoogleTts, useVoiceRssTts, useIvonaTts, accessKey, secretKey):
        self.noteType = noteType
        self.useSpell = useSpell
        self.useGoogleTts = useGoogleTts
        self.useVoiceRssTts = useVoiceRssTts
        self.useIvonaTts = useIvonaTts
        self.accessKey = accessKey
        self.secretKey = secretKey

    def loadPluginData(self, profileFolder):
        dataFile = os.path.join(profileFolder, MEDIA_FOLDER_NAME, DATA_FILE_NAME)
        if(os.path.isfile(dataFile)):
            inFile = None
            pluginData = None

            with open(dataFile, "rb") as inFile:
                pluginData = pickle.load(inFile)
                self.updateFromDict(pluginData)

    def savePluginData(self, profileFolder):
        pluginData = self.__dict__
        mediaDir = os.path.join(profileFolder, MEDIA_FOLDER_NAME)
        dataFile = os.path.join(mediaDir, DATA_FILE_NAME)
        if not os.path.exists(mediaDir):
            os.mkdir(mediaDir)

        with open(dataFile, "wb") as outFile:
            pickle.dump(pluginData, outFile)
