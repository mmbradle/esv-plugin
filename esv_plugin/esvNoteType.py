'''
Created on Sep 3, 2014

@author: Mike
'''
import logging
from os.path import os
import urllib

from esv_plugin.esv.esvSession import EsvSession
from esv_plugin.esv.verse import ScriptureReference
from esv_plugin.tts import ivonaTts
from esv_plugin.tts.googleTtsWrapper import GoogleTtsWrapper
from esv_plugin.tts.voiceRssWrapper import VoiceRssWrapper
from esv_plugin.tts.substitution import Substitution


LOG = logging.getLogger(__name__)

REF_TEXT_FIELD = 'Reference'
VER_TEXT_FIELD = 'VerseText'
HINT_FIELD = 'Hint'
VER_AUDIO_FIELD = 'VerseAudio'
REF_AUDIO_FIELD = 'ReferenceAudio'
IMAGE_FIELD = 'Image'
NOTES_FIELD = 'Notes'
RECOGNIZE_FIELD = 'Create "Recognize" Card?'
RECITE_FIELD = 'Create "Recite" Card?'

FIELDS = [
          REF_TEXT_FIELD,
          VER_TEXT_FIELD,
          VER_AUDIO_FIELD,
          HINT_FIELD,
          REF_AUDIO_FIELD,
          IMAGE_FIELD,
          NOTES_FIELD,
          RECOGNIZE_FIELD,
          RECITE_FIELD
          ]

format_dict = {
        'REF_TEXT_FIELD': REF_TEXT_FIELD,
        'VER_TEXT_FIELD': VER_TEXT_FIELD,
        'VER_AUDIO_FIELD': VER_AUDIO_FIELD,
        'HINT_FIELD': HINT_FIELD,
        'REF_AUDIO_FIELD': REF_AUDIO_FIELD,
        'IMAGE_FIELD': IMAGE_FIELD,
        'NOTES_FIELD': NOTES_FIELD,
        'RECOGNIZE_FIELD': RECOGNIZE_FIELD,
        'RECITE_FIELD': RECITE_FIELD
      }

esv_lookup_front_template = '''
{{{{#{HINT_FIELD}}}}}
<div class={HINT_FIELD}>{{{{{HINT_FIELD}}}}}</div>{{{{/{HINT_FIELD}}}}}
<div class={VER_TEXT_FIELD}>{{{{{VER_TEXT_FIELD}}}}}</div>

{{{{{VER_AUDIO_FIELD}}}}}
'''.format(**format_dict).lstrip('\n')

esv_lookup_back_template = '''
{{{{#{HINT_FIELD}}}}}<div class={HINT_FIELD}>{{{{{HINT_FIELD}}}}}</div>{{{{/{HINT_FIELD}}}}}
<div class={VER_TEXT_FIELD}>{{{{{VER_TEXT_FIELD}}}}}</div>

<hr id=answer>
{{{{{REF_AUDIO_FIELD}}}}}
<div class={REF_TEXT_FIELD}>{{{{{REF_TEXT_FIELD}}}}}</div>
{{{{{IMAGE_FIELD}}}}}
'''.format(**format_dict).lstrip('\n')

esv_recognize_front_template = '''
{{{{#{RECOGNIZE_FIELD}}}}}
<div>Recognize:</div>
<div class={REF_TEXT_FIELD}>{{{{{REF_TEXT_FIELD}}}}}</div>
{{{{{REF_AUDIO_FIELD}}}}}
{{{{/{RECOGNIZE_FIELD}}}}}
'''.format(**format_dict).lstrip('\n')

esv_recognize_back_template = '''
<div>Recognize:</div>
<div class={REF_TEXT_FIELD}>{{{{{REF_TEXT_FIELD}}}}}</div>

<hr id=answer>

{{{{#{HINT_FIELD}}}}}<div class={HINT_FIELD}>{{{{{HINT_FIELD}}}}}</div>{{{{/{HINT_FIELD}}}}}
<div class={VER_TEXT_FIELD}>{{{{{VER_TEXT_FIELD}}}}}</div>
<div style="margin-top: 10px">{{{{{IMAGE_FIELD}}}}}</div>
{{{{{VER_AUDIO_FIELD}}}}}
{{{{{REF_AUDIO_FIELD}}}}}
'''.format(**format_dict).lstrip('\n')

esv_recite_front_template = '''
{{{{#{RECITE_FIELD}}}}}
<div>Recite:</div>
<div class={REF_TEXT_FIELD}>{{{{{REF_TEXT_FIELD}}}}}</div>
{{{{hint:{HINT_FIELD}}}}}<br><br>
{{{{type:{VER_TEXT_FIELD}}}}}
{{{{{REF_AUDIO_FIELD}}}}}
{{{{/{RECITE_FIELD}}}}}
'''.format(**format_dict).lstrip('\n')

esv_recite_back_template = '''
{{{{type:{VER_TEXT_FIELD}}}}}
<div class={REF_TEXT_FIELD}>{{{{{REF_TEXT_FIELD}}}}}</div>
<div class={VER_TEXT_FIELD}>{{{{{VER_TEXT_FIELD}}}}}</div>
<div class={IMAGE_FIELD}>{{{{{IMAGE_FIELD}}}}}</div>
{{{{{VER_AUDIO_FIELD}}}}}{{{{{REF_AUDIO_FIELD}}}}}<br>
'''.format(**format_dict).lstrip('\n')

esv_styling_template = '''
.card {{
 font-family: Georgia, "Times New Roman", Times, serif;
 font-size: 20px;
 text-align: center;
 color: black;
}}
.card1 {{
 background-color: #D8B4B6;
}}
.card2 {{
 background-color: #DDDEEF;
}}
.card3 {{
 background-color: #DEFADC;
}}
.{VER_TEXT_FIELD}{{
text-align:left;
font-size: 12pt;
line-height: 130%;
}}
.{REF_TEXT_FIELD}{{
 margin-top: .4em;
 margin-bottom: .4em;
 font-size: 24px;
}}
.{IMAGE_FIELD}{{
 margin-top: 1em;
}}
.{HINT_FIELD}{{
font-weight: bold;
font-size: .8em;
margin-bottom: 1em;
}}
.hint{{
font-weight: bold;
font-size: .8em;
margin-bottom: 1em;
}}
#rightanswer {{
 display: none;
}}
'''.format(**format_dict).lstrip('\n')

esv_templates = {
                 'Lookup': {'front': esv_lookup_front_template, 'back': esv_lookup_back_template},
                 'Recognize': {'front': esv_recognize_front_template, 'back': esv_recognize_back_template},
                 'Recite': {'front': esv_recite_front_template, 'back': esv_recite_back_template}
                 }


def populate_note(editor, esv_options, createTemporaryFile, esv_session=EsvSession()):
    '''
    Lookup verse from ESV web-service and set results in current note.
    '''
    reference_text = editor.note[REF_TEXT_FIELD]

    # set reference
    try:
        LOG.debug('Setting %s', REF_TEXT_FIELD)
        full_reference = esv_session.getFullReference(reference_text)
    except:
        LOG.exception('Error while setting %s.', REF_TEXT_FIELD)
        raise
    if not full_reference:
        raise ValueError("No such passage: %s" % (editor.note[REF_TEXT_FIELD]))
    editor.note[REF_TEXT_FIELD] = full_reference

    # Set verse audio
    if not editor.note[VER_AUDIO_FIELD]:
        try:
            LOG.debug('Setting %s', VER_AUDIO_FIELD)
            mp3Link = esv_session.getMp3Link(full_reference)
            verseMp3FileName = ScriptureReference.parseRefText(full_reference).getRefAsFile('mp3', 'ver')

            # Retrieve audio into a temporary folder
            audio_path = createTemporaryFile(os.path.basename(verseMp3FileName), True)
            urllib.urlretrieve(mp3Link, audio_path)

            # pylint: disable=protected-access
            # Need to use the protected version as it returns and the public version does not
            editor.note[VER_AUDIO_FIELD] = editor._addMedia(audio_path)
        except:
            LOG.exception('Error while setting %s.', VER_AUDIO_FIELD)
    else:
        LOG.debug('Skipping setting %s. Already contains: "%s".', VER_AUDIO_FIELD, editor.note[VER_AUDIO_FIELD])

    # set verse text
    if not editor.note[VER_TEXT_FIELD]:
        try:
            LOG.debug('Setting %s', VER_TEXT_FIELD)
            editor.note[VER_TEXT_FIELD] = esv_session.getVerseContents(full_reference)
        except:
            LOG.exception('Error while setting %s.', VER_TEXT_FIELD)
    else:
        LOG.debug('Skipping setting %s. Already contains: "%s".', VER_TEXT_FIELD, editor.note[VER_TEXT_FIELD])

    # set verse reference_text audio
    if not editor.note[REF_AUDIO_FIELD]:
        try:
            refAudioText = ""
            mp3 = None
            if (esv_options.useGoogleTts == True):
                LOG.debug('Setting %s with Google TTS', REF_AUDIO_FIELD)
                refTextToConvert = ScriptureReference.parseRefText(full_reference).getRefForTts()
                tts = GoogleTtsWrapper(refTextToConvert, "en-us")
                mp3 = tts.fetch()
            elif (esv_options.useVoiceRssTts == True):
                LOG.debug('Setting %s with VoiceRss TTS', REF_AUDIO_FIELD)
                refTextToConvert = ScriptureReference.parseRefText(full_reference).getRefForTts()
                tts = VoiceRssWrapper(refTextToConvert, "en-us")
                mp3 = tts.fetch()
            elif (esv_options.useIvonaTts == True):
                LOG.debug('Setting %s with Ivona TTS', REF_AUDIO_FIELD)
                refTextToConvert = ScriptureReference.parseRefText(full_reference).getRefForTts(Substitution())
                tts = ivonaTts.create_voice(esv_options.accessKey, esv_options.secretKey)
                mp3 = tts.fetch(refTextToConvert)
            else:  # None
                LOG.debug('Skipping setting %s', REF_AUDIO_FIELD)
#                 refAudioText = cgi.escape(refTextToConvert)

            if mp3:
                # Retrive audio into a temporary folder
                refMp3FileName = ScriptureReference.parseRefText(full_reference).getRefAsFile('mp3', 'ref')
                audio_path = createTemporaryFile(os.path.basename(refMp3FileName), True)

                if audio_path:
                    open(audio_path, "wb").write(mp3)
                    # pylint: disable=protected-access
                    # Need to use the protected version as it returns and the public version does not
                    refAudioText = editor._addMedia(audio_path)

            editor.note[REF_AUDIO_FIELD] = refAudioText
        except:
            LOG.exception('Error while setting %s.', REF_AUDIO_FIELD)
    else:
        LOG.debug('Skipping setting %s. Already contains: "%s".', REF_AUDIO_FIELD, editor.note[REF_AUDIO_FIELD])

    # set title/hint
    if (not editor.note[HINT_FIELD]) and (editor.note[VER_TEXT_FIELD]):
        LOG.debug('Setting %s', HINT_FIELD)
        editor.note[HINT_FIELD] = ",".join(editor.note[VER_TEXT_FIELD].split()[:5]).replace(",", " ")
    else:
        LOG.debug('Skipping setting %s. Already contains: "%s".', HINT_FIELD, editor.note[HINT_FIELD])

    # set 'y' for any additional card types that need to be generated
    if not editor.note[RECOGNIZE_FIELD]:
        LOG.debug('Setting %s', RECOGNIZE_FIELD)
        editor.note[RECOGNIZE_FIELD] = 'y'
    else:
        LOG.debug('Skipping setting %s. Already contains: "%s".', RECOGNIZE_FIELD, editor.note[RECOGNIZE_FIELD])

    # reload note, makes things show up
    editor.loadNote()


def add_to_model(note_type_name, anki_models):
    model = anki_models.byName(note_type_name)
    if not model:
        model = __add_note_type(note_type_name, anki_models)
    return model


def __add_note_type(note_type_name, anki_models):
    # Create and add Note Type
    anki_verse_model = anki_models.new(note_type_name)
    anki_models.add(anki_verse_model)

    # Setup CSS styling
    anki_verse_model['css'] = esv_styling_template

    # Add fields:
    for field_name in FIELDS:
        field = anki_models.newField(field_name)
        anki_models.addField(anki_verse_model, field)

    # Add templates
    for template_name, template_dict in esv_templates.iteritems():
        template = anki_models.newTemplate(template_name)
        template['qfmt'] = template_dict['front']
        template['afmt'] = template_dict['back']
        anki_models.addTemplate(anki_verse_model, template)

    return anki_verse_model
