import logging
import sys

from PyQt4 import QtGui
from PyQt4.QtCore import QTimer

from esv_plugin import esvNoteType
from esv_plugin.esvOptions import EsvOptions
from esv_plugin.ui.Ui_EsvOptions import Ui_EsvOptions


LOG = logging.getLogger(__name__)


class EsvOptionsWidget(QtGui.QWidget):
    def __init__(self, esv_options, anki_models):
        super(EsvOptionsWidget, self).__init__()

        # Setup passed-in member data
        self.esvOptions = esv_options
        self.anki_models = anki_models
        assert isinstance(self.esvOptions, EsvOptions)

        # Create and setup the UI
        self.ui = Ui_EsvOptions()
        self.ui.setupUi(self)

        # Update the UI from the Model
        self.transformFromModel()

        # Make sure dialog is the correct size.
        self.updateGroupVisibility()
        self.fixSize()

        # When Ivona radio button is selected/deselected, call a function to show/hide the btn-group.
        self.ui.ttsIvonaBtn.toggled.connect(self.updateGroupVisibility)

        # Update enabled/disabled
        self.updateEnabledWidgets(self.ui.noteTypeNameLineEdit.text())

    def getEsvOptions(self):
        return self.esvOptions

    def updateGroupVisibility(self):
        self.ui.ivonaGroup.setVisible(self.ui.ttsIvonaBtn.isChecked())
        QTimer.singleShot(100, lambda: self.fixSize())  # pylint: disable=unnecessary-lambda

    def fixSize(self):
        self.resize(self.minimumSizeHint())

    def transformToModel(self):
        self.esvOptions.update(
                               self.ui.noteTypeNameLineEdit.text(),
                               self.ui.useSpellCheckBox.isChecked(),
                               self.ui.ttsGoogleBtn.isChecked(),
                               self.ui.ttsVoiceRssBtn.isChecked(),
                               self.ui.ttsIvonaBtn.isChecked(),
                               str(self.ui.ivonaAccessKeyText.text()),
                               str(self.ui.ivonaSecretKeyText.toPlainText())
                               )

    def transformFromModel(self):
        self.ui.noteTypeNameLineEdit.setText(self.esvOptions.noteType)
        self.ui.useSpellCheckBox.setChecked(self.esvOptions.useSpell)
        self.ui.ttsNoneBtn.setChecked(self.esvOptions.useGoogleTts == False and self.esvOptions.useGoogleTts == False)
        self.ui.ttsGoogleBtn.setChecked(self.esvOptions.useGoogleTts)
        self.ui.ttsVoiceRssBtn.setChecked(self.esvOptions.useVoiceRssTts)
        self.ui.ttsIvonaBtn.setChecked(self.esvOptions.useIvonaTts)
        self.ui.ivonaAccessKeyText.setText(self.esvOptions.accessKey)
        self.ui.ivonaSecretKeyText.setText(self.esvOptions.secretKey)

    def accept(self):
        '''Execute the command in response to the OK button.'''
        try:
            LOG.info('Accept ESV Options Widget')
            self.transformToModel()
            self.close()
        except StandardError:
            LOG.exception('Error caught while accepting ESV Options Widget')
            raise

    def reject(self):
        '''Cancel.'''
        try:
            LOG.info('Reject ESV Options Widget')
            self.close()
        except StandardError:
            LOG.exception('Error caught while rejecting ESV Options Widget')
            raise

    def createNoteType(self):
        '''Create.'''
        note_type_name = self.ui.noteTypeNameLineEdit.text()
        try:
            LOG.info("Handling create button pressed")
            esvNoteType.add_to_model(note_type_name, self.anki_models)
        except StandardError:
            LOG.exception('Error caught while handling create button pressed')
            raise
        finally:
            self.updateEnabledWidgets(note_type_name)

    def updateEnabledWidgets(self, note_type_name):
        '''If model exists, disable button, else enable'''
        try:
            LOG.debug("Handle note type text update")
            if self.anki_models.byName(note_type_name):
                self.ui.createButton.setDisabled(True)
                self.ui.noteTypeNameLineEdit.setDisabled(True)
            else:
                self.ui.createButton.setEnabled(True)
                self.ui.noteTypeNameLineEdit.setEnabled(True)
        except StandardError:
            LOG.exception('Error caught while updating note type text')
            raise

if __name__ == "__main__":
    qt_app = QtGui.QApplication(sys.argv)

    main_esv_options = EsvOptions()
    main_esv_options.useSpell = True
    main_esv_options.useIvonaTts = True
    main_esv_options.accessKey = "access key"
    main_esv_options.secretKey = "secret key"

    myapp = EsvOptionsWidget(main_esv_options)
    myapp.show()
    sys.exit(qt_app.exec_())
