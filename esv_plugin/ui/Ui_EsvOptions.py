# pylint: skip-file
# @PydevCodeAnalysisIgnore
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_EsvOptions.ui'
#
# Created: Fri Oct 09 22:22:00 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_EsvOptions(object):
    def setupUi(self, EsvOptions):
        EsvOptions.setObjectName(_fromUtf8("EsvOptions"))
        EsvOptions.resize(288, 388)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(EsvOptions.sizePolicy().hasHeightForWidth())
        EsvOptions.setSizePolicy(sizePolicy)
        self.formLayout = QtGui.QFormLayout(EsvOptions)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.ttsGroup = QtGui.QGroupBox(EsvOptions)
        self.ttsGroup.setObjectName(_fromUtf8("ttsGroup"))
        self.gridLayout = QtGui.QGridLayout(self.ttsGroup)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.ttsNoneBtn = QtGui.QRadioButton(self.ttsGroup)
        self.ttsNoneBtn.setObjectName(_fromUtf8("ttsNoneBtn"))
        self.gridLayout.addWidget(self.ttsNoneBtn, 0, 0, 1, 1)
        self.ttsGoogleBtn = QtGui.QRadioButton(self.ttsGroup)
        self.ttsGoogleBtn.setObjectName(_fromUtf8("ttsGoogleBtn"))
        self.gridLayout.addWidget(self.ttsGoogleBtn, 1, 0, 1, 1)
        self.ttsIvonaBtn = QtGui.QRadioButton(self.ttsGroup)
        self.ttsIvonaBtn.setObjectName(_fromUtf8("ttsIvonaBtn"))
        self.gridLayout.addWidget(self.ttsIvonaBtn, 3, 0, 1, 1)
        self.ttsVoiceRssBtn = QtGui.QRadioButton(self.ttsGroup)
        self.ttsVoiceRssBtn.setObjectName(_fromUtf8("ttsVoiceRssBtn"))
        self.gridLayout.addWidget(self.ttsVoiceRssBtn, 2, 0, 1, 1)
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.ttsGroup)
        self.ivonaGroup = QtGui.QGroupBox(EsvOptions)
        self.ivonaGroup.setEnabled(True)
        self.ivonaGroup.setObjectName(_fromUtf8("ivonaGroup"))
        self.formLayout_2 = QtGui.QFormLayout(self.ivonaGroup)
        self.formLayout_2.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout_2.setObjectName(_fromUtf8("formLayout_2"))
        self.ivonaSecretKeyLabel = QtGui.QLabel(self.ivonaGroup)
        self.ivonaSecretKeyLabel.setObjectName(_fromUtf8("ivonaSecretKeyLabel"))
        self.formLayout_2.setWidget(1, QtGui.QFormLayout.LabelRole, self.ivonaSecretKeyLabel)
        self.ivonaAccessKeyLabel = QtGui.QLabel(self.ivonaGroup)
        self.ivonaAccessKeyLabel.setObjectName(_fromUtf8("ivonaAccessKeyLabel"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.LabelRole, self.ivonaAccessKeyLabel)
        self.ivonaAccessKeyText = QtGui.QLineEdit(self.ivonaGroup)
        self.ivonaAccessKeyText.setMinimumSize(QtCore.QSize(190, 0))
        self.ivonaAccessKeyText.setObjectName(_fromUtf8("ivonaAccessKeyText"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.FieldRole, self.ivonaAccessKeyText)
        self.ivonaSecretKeyText = QtGui.QTextEdit(self.ivonaGroup)
        self.ivonaSecretKeyText.setMaximumSize(QtCore.QSize(16777215, 81))
        self.ivonaSecretKeyText.setObjectName(_fromUtf8("ivonaSecretKeyText"))
        self.formLayout_2.setWidget(1, QtGui.QFormLayout.FieldRole, self.ivonaSecretKeyText)
        self.formLayout.setWidget(6, QtGui.QFormLayout.SpanningRole, self.ivonaGroup)
        self.buttonBox = QtGui.QDialogButtonBox(EsvOptions)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.formLayout.setWidget(7, QtGui.QFormLayout.LabelRole, self.buttonBox)
        self.useSpellCheckBox = QtGui.QCheckBox(EsvOptions)
        self.useSpellCheckBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.useSpellCheckBox.setText(_fromUtf8(""))
        self.useSpellCheckBox.setObjectName(_fromUtf8("useSpellCheckBox"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.useSpellCheckBox)
        self.useSpellLabel = QtGui.QLabel(EsvOptions)
        self.useSpellLabel.setObjectName(_fromUtf8("useSpellLabel"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.useSpellLabel)
        self.noteTypeGroup = QtGui.QGroupBox(EsvOptions)
        self.noteTypeGroup.setObjectName(_fromUtf8("noteTypeGroup"))
        self.gridLayout_2 = QtGui.QGridLayout(self.noteTypeGroup)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.noteTypeNameLineEdit = QtGui.QLineEdit(self.noteTypeGroup)
        self.noteTypeNameLineEdit.setObjectName(_fromUtf8("noteTypeNameLineEdit"))
        self.gridLayout_2.addWidget(self.noteTypeNameLineEdit, 0, 0, 1, 1)
        self.createButton = QtGui.QPushButton(self.noteTypeGroup)
        self.createButton.setEnabled(True)
        self.createButton.setIconSize(QtCore.QSize(16, 16))
        self.createButton.setObjectName(_fromUtf8("createButton"))
        self.gridLayout_2.addWidget(self.createButton, 0, 1, 1, 1)
        self.formLayout.setWidget(1, QtGui.QFormLayout.SpanningRole, self.noteTypeGroup)

        self.retranslateUi(EsvOptions)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), EsvOptions.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), EsvOptions.reject)
        QtCore.QObject.connect(self.createButton, QtCore.SIGNAL(_fromUtf8("clicked()")), EsvOptions.createNoteType)
        QtCore.QObject.connect(self.noteTypeNameLineEdit, QtCore.SIGNAL(_fromUtf8("textChanged(QString)")), EsvOptions.updateEnabledWidgets)
        QtCore.QMetaObject.connectSlotsByName(EsvOptions)

    def retranslateUi(self, EsvOptions):
        EsvOptions.setWindowTitle(_translate("EsvOptions", "Dialog", None))
        self.ttsGroup.setTitle(_translate("EsvOptions", "TTS Engine", None))
        self.ttsNoneBtn.setText(_translate("EsvOptions", "None", None))
        self.ttsGoogleBtn.setText(_translate("EsvOptions", "Google TTS", None))
        self.ttsIvonaBtn.setText(_translate("EsvOptions", "Ivona", None))
        self.ttsVoiceRssBtn.setText(_translate("EsvOptions", "VoiceRss TTS", None))
        self.ivonaGroup.setTitle(_translate("EsvOptions", "Ivona Options", None))
        self.ivonaSecretKeyLabel.setText(_translate("EsvOptions", "Secret Key", None))
        self.ivonaAccessKeyLabel.setText(_translate("EsvOptions", "Access Key", None))
        self.useSpellLabel.setText(_translate("EsvOptions", "Use Improved Spell Checking", None))
        self.noteTypeGroup.setTitle(_translate("EsvOptions", "ESV Note Type", None))
        self.noteTypeNameLineEdit.setText(_translate("EsvOptions", "Esv Verse", None))
        self.createButton.setText(_translate("EsvOptions", "Create", None))

