import urllib
import urllib2

class VoiceRssWrapper:
    """ VoiceRssWrapper: an interface to VoiceRssWrapper's Text to Speech API """

    BASE_URL = 'http://api.voicerss.org'
    LANGUAGES = {
        'ca-es' : 'Catalan',
        'zh-cn' : 'Chinese (China)',
        'zh-hk' : 'Chinese (Hong Kong)',
        'zh-tw' : 'Chinese (Taiwan)',
        'da-dk' : 'Danish',
        'nl-nl' : 'Dutch',
        'en-au' : 'English (Australia)',
        'en-ca' : 'English (Canada)',
        'en-gb' : 'English (Great Britain)',
        'en-in' : 'English (India)',
        'en-us' : 'English (United States)',
        'fi-fi' : 'Finnish',
        'fr-ca' : 'French (Canada)',
        'fr-fr' : 'French (France)',
        'de-de' : 'German',
        'it-it' : 'Italian',
        'ja-jp' : 'Japanese',
        'ko-kr' : 'Korean',
        'nb-no' : 'Norwegian',
        'pl-pl' : 'Polish',
        'pt-br' : 'Portuguese (Brazil)',
        'pt-pt' : 'Portuguese (Portugal)',
        'ru-ru' : 'Russian',
        'es-mx' : 'Spanish (Mexico)',
        'es-es' : 'Spanish (Spain)',
        'sv-se' : 'Swedish (Sweden)'
    }

    def __init__(self, text, lang = 'en-us'):
        if lang.lower() not in self.LANGUAGES:
            raise Exception('Language not supported: %s' % lang)
        else:
            self.lang = lang.lower()

        if not text:
            raise Exception('No text to speak')
        else:
            self.text = text

    def fetch(self):
        payload = { 'key' : 'db0dca6538cd4640bee1544bac3a0aed',
                    'src' : self.text,
#                     'ssml' : 'true',
                    'hl' : self.lang }

        try:
            data = urllib.urlencode(payload)
            request = urllib2.Request(self.BASE_URL, data)
            response = urllib2.urlopen(request)
            return response.read()
        except IOError as io_error:
            raise io_error

if __name__ == "__main__":
        pass
