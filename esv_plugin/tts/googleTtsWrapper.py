import urllib
import urllib2

class GoogleTtsWrapper:
    """ GoogleTtsWrapper (Google Text to Speech): an interface to Google's Text to Speech API """

    BASE_URL = 'http://translate.google.com/translate_tts'
    LANGUAGES = {
        'af' : 'Afrikaans',
        'sq' : 'Albanian',
        'ar' : 'Arabic',
        'hy' : 'Armenian',
        'ca' : 'Catalan',
        'zh' : 'Chinese',
        'zh-cn' : 'Chinese (Mandarin/China)',
        'zh-tw' : 'Chinese (Mandarin/Taiwan)',
        'zh-yue' : 'Chinese (Cantonese)',
        'hr' : 'Croatian',
        'cs' : 'Czech',
        'da' : 'Danish',
        'nl' : 'Dutch',
        'en' : 'English',
        'en-au' : 'English (Australia)',
        'en-uk' : 'English (United Kingdom)',
        'en-us' : 'English (United States)',
        'eo' : 'Esperanto',
        'fi' : 'Finnish',
        'fr' : 'French',
        'de' : 'German',
        'el' : 'Greek',
        'ht' : 'Haitian Creole',
        'hi' : 'Hindi',
        'hu' : 'Hungarian',
        'is' : 'Icelandic',
        'id' : 'Indonesian',
        'it' : 'Italian',
        'ja' : 'Japanese',
        'ko' : 'Korean',
        'la' : 'Latin',
        'lv' : 'Latvian',
        'mk' : 'Macedonian',
        'no' : 'Norwegian',
        'pl' : 'Polish',
        'pt' : 'Portuguese',
        'pt-br' : 'Portuguese (Brazil)',
        'ro' : 'Romanian',
        'ru' : 'Russian',
        'sr' : 'Serbian',
        'sk' : 'Slovak',
        'es' : 'Spanish',
        'es-es' : 'Spanish (Spain)',
        'es-us' : 'Spanish (United States)',
        'sw' : 'Swahili',
        'sv' : 'Swedish',
        'ta' : 'Tamil',
        'th' : 'Thai',
        'tr' : 'Turkish',
        'vi' : 'Vietnamese',
        'cy' : 'Welsh'
    }

    def __init__(self, text, lang = 'en'):
        if lang.lower() not in self.LANGUAGES:
            raise Exception('Language not supported: %s' % lang)
        else:
            self.lang = lang.lower()

        if not text:
            raise Exception('No text to speak')
        else:
            self.text = text

    def fetch(self):
        """ Do the Web request and save to a file-like object """
        payload = { 'ie' : 'UTF-8',
                    'client' : 't',
                    'tl' : self.lang,
                    'q' : self.text,
                    'key' : 'AIzaSyAL-1O_y_P-Ethi3kFue7ac55LU4cHk240' }
        try:
            data = urllib.urlencode(payload)
            request = urllib2.Request(self.BASE_URL, data)
            response = urllib2.urlopen(request)
            return response.read()
                
        except IOError as io_error:
            raise IOError(
                "Google Translate returned an HTTP 503 (Service Unavailable) "
                "error. Unless Google Translate is down, this might indicate "
                "that too many TTS requests have recently come from your IP "
                "address. If so, try again after 24 hours.\n"
                "\n"
                "Depending on your specific situation, you might be able to "
                "switch to a different service offering."
            ) if hasattr(io_error, 'code') and io_error.code == 503 \
                else io_error

if __name__ == "__main__":
        pass
