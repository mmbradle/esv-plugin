from nose.tools.trivial import eq_

from esv_plugin import esvNoteType
from mockito import mock, when, verifyNoMoreInteractions, verify, any
from esv_plugin.esvOptions import EsvOptions
from esv_plugin.esv.esvSession import EsvSession


def test_print_templates():
    ''' Prints out templates to make sure they look correct '''
    eq_('{{#Hint}}\n<div class=Hint>{{Hint}}</div>{{/Hint}}\n<div class=VerseText>{{VerseText}}</div>\n\n{{VerseAudio}}\n', esvNoteType.esv_lookup_front_template)  # @IgnorePep8
    eq_('{{#Hint}}<div class=Hint>{{Hint}}</div>{{/Hint}}\n<div class=VerseText>{{VerseText}}</div>\n\n<hr id=answer>\n{{ReferenceAudio}}\n<div class=Reference>{{Reference}}</div>\n{{Image}}\n', esvNoteType.esv_lookup_back_template)  # @IgnorePep8
    eq_('{{#Create "Recognize" Card?}}\n<div>Recognize:</div>\n<div class=Reference>{{Reference}}</div>\n{{ReferenceAudio}}\n{{/Create "Recognize" Card?}}\n', esvNoteType.esv_recognize_front_template)  # @IgnorePep8
    eq_('<div>Recognize:</div>\n<div class=Reference>{{Reference}}</div>\n\n<hr id=answer>\n\n{{#Hint}}<div class=Hint>{{Hint}}</div>{{/Hint}}\n<div class=VerseText>{{VerseText}}</div>\n<div style="margin-top: 10px">{{Image}}</div>\n{{VerseAudio}}\n{{ReferenceAudio}}\n', esvNoteType.esv_recognize_back_template)  # @IgnorePep8
    eq_('{{#Create "Recite" Card?}}\n<div>Recite:</div>\n<div class=Reference>{{Reference}}</div>\n{{hint:Hint}}<br><br>\n{{type:VerseText}}\n{{ReferenceAudio}}\n{{/Create "Recite" Card?}}\n', esvNoteType.esv_recite_front_template)  # @IgnorePep8
    eq_('{{type:VerseText}}\n<div class=Reference>{{Reference}}</div>\n<div class=VerseText>{{VerseText}}</div>\n<div class=Image>{{Image}}</div>\n{{VerseAudio}}{{ReferenceAudio}}<br>\n', esvNoteType.esv_recite_back_template)  # @IgnorePep8
    eq_('.card {\n font-family: Georgia, "Times New Roman", Times, serif;\n font-size: 20px;\n text-align: center;\n color: black;\n}\n.card1 {\n background-color: #D8B4B6;\n}\n.card2 {\n background-color: #DDDEEF;\n}\n.card3 {\n background-color: #DEFADC;\n}\n.VerseText{\ntext-align:left;\nfont-size: 12pt;\nline-height: 130%;\n}\n.Reference{\n margin-top: .4em;\n margin-bottom: .4em;\n font-size: 24px;\n}\n.Image{\n margin-top: 1em;\n}\n.Hint{\nfont-weight: bold;\nfont-size: .8em;\nmargin-bottom: 1em;\n}\n.hint{\nfont-weight: bold;\nfont-size: .8em;\nmargin-bottom: 1em;\n}\n#rightanswer {\n display: none;\n}\n', esvNoteType.esv_styling_template)  # @IgnorePep8


def test_populate_note():
    # Setup
    editor = mock()
    editor.note = {
            esvNoteType.REF_TEXT_FIELD: 'jn 3:16',
            esvNoteType.VER_TEXT_FIELD: '',
            esvNoteType.VER_AUDIO_FIELD: '',
            esvNoteType.HINT_FIELD: '',
            esvNoteType.REF_AUDIO_FIELD: '',
            esvNoteType.IMAGE_FIELD: '',
            esvNoteType.NOTES_FIELD: '',
            esvNoteType.RECOGNIZE_FIELD: '',
            esvNoteType.RECITE_FIELD: ''
    }
    when(editor)._addMedia(any(str)).thenReturn('[audio.mp3]')  # pylint: disable=protected-access

    esv_options = mock(EsvOptions)

    esv_session = mock(EsvSession)
    when(esv_session).getFullReference(any(str)).thenReturn('John 3:16')
    when(esv_session).getMp3Link('John 3:16').thenReturn('http://foo.bar.com')
    when(esv_session).getVerseContents('John 3:16').thenReturn('For God so loved the world')

    import urllib
    when(urllib).urlretrieve(any(), any()).thenReturn(None)

    def createTemporaryFile(a, b):  # @IgnorePep8
        print b
        return 'c:/tmp/{}'.format(a)

    # When
    esvNoteType.populate_note(editor, esv_options, createTemporaryFile, esv_session)

    # Then
    eq_('John 3:16', editor.note[esvNoteType.REF_TEXT_FIELD])
    eq_('For God so loved the world', editor.note[esvNoteType.VER_TEXT_FIELD])
    eq_('[audio.mp3]', editor.note[esvNoteType.VER_AUDIO_FIELD])
    eq_('For God so loved the', editor.note[esvNoteType.HINT_FIELD])
    eq_('', editor.note[esvNoteType.REF_AUDIO_FIELD])
    eq_('', editor.note[esvNoteType.IMAGE_FIELD])
    eq_('', editor.note[esvNoteType.NOTES_FIELD])
    eq_('y', editor.note[esvNoteType.RECOGNIZE_FIELD])
    eq_('', editor.note[esvNoteType.RECITE_FIELD])


def test_add_to_model():
    # Setup
    note_type_name = 'note_type_name'
    anki_models = mock()
    anki_verse_model = {}

    when(anki_models).byName(note_type_name).thenReturn(None)
    when(anki_models).new(note_type_name).thenReturn(anki_verse_model)
    when(anki_models).newField(any(str)).thenReturn({'a': 'field'})
    when(anki_models).newTemplate(any(str)).thenReturn({'qfmt': 'template1', 'afmt': 'template2'})
    # addTemplate

    # When
    model = esvNoteType.add_to_model(note_type_name, anki_models)

    # Then
    eq_(anki_verse_model, model)
    eq_(esvNoteType.esv_styling_template, anki_verse_model['css'])
    verify(anki_models).byName(note_type_name)
    verify(anki_models).new(note_type_name)
    verify(anki_models).add(anki_verse_model)
    verify(anki_models, times=9).newField(any(str))
    verify(anki_models, times=9).addField(any(), any())
    verify(anki_models, times=9).addField(any(), any())
    verify(anki_models, times=3).newTemplate(any(str))
    verify(anki_models, times=3).addTemplate(any(), any())
    verifyNoMoreInteractions(anki_models)


def test_add_to_model_AlreadyExists():
    # Setup
    note_type_name = 'note_type_name'
    anki_models = mock()
    when(anki_models).byName(note_type_name).thenReturn('verify')

    # When
    model = esvNoteType.add_to_model(note_type_name, anki_models)

    # Then
    eq_('verify', model)
    verify(anki_models).byName(note_type_name)
    verifyNoMoreInteractions(anki_models)
