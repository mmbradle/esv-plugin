from contextlib import contextmanager
from os.path import os
import shutil
import tempfile

from nose.tools import eq_

from esv_plugin.esvOptions import EsvOptions, DATA_FILE_NAME, MEDIA_FOLDER_NAME


@contextmanager
def TemporaryDirectory():
    '''
    from http://stackoverflow.com/questions/6884991/how-to-delete-dir-created-by-python-tempfile-mkdtemp
    '''
    name = tempfile.mkdtemp()
    yield name
    try:
        shutil.rmtree(name)
    except OSError as e:
        # Reraise unless code 2: no such file or directory
        if e.errno != 2:
            raise


def test_savePluginData():
    ''' Also tests loadPluginData '''
    with TemporaryDirectory() as temp_dir:
        # Setup
        main_esv_options = EsvOptions()
        main_esv_options.secretKey = "123"

        # When
        main_esv_options.savePluginData(temp_dir)

        # Then
        assert (os.path.isfile(os.path.join(temp_dir, MEDIA_FOLDER_NAME, DATA_FILE_NAME)))

        # Setup
        main_esv_options.secretKey = "should be over written"

        # When
        main_esv_options.loadPluginData(temp_dir)

        # Then
        eq_(main_esv_options.secretKey, "123")


def test_updateFromDict():
    # Setup
    esvOptions1 = EsvOptions()
    esvOptions1.noteType = "My Note Type"
    esvOptions1.useSpell = False
    dictionary = esvOptions1.__dict__
    esvOptions2 = EsvOptions()
    esvOptions2.useIvonaTts = True

    # When
    esvOptions2.updateFromDict(dictionary)

    # Then
    eq_(esvOptions1.__dict__, esvOptions2.__dict__)
