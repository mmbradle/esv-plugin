from nose.tools import eq_

from esv_plugin.esv.verse import ScriptureReference
from esv_plugin.tts.substitution import Substitution


def test_parseRefText():
    result = ScriptureReference.parseRefText("1 John 1:12")
    eq_("1 John", result.book)
    eq_("1", result.chapter)
    eq_("12", result.verse)

    result = ScriptureReference.parseRefText("Jude 2")
    eq_("Jude", result.book)
    eq_(None, result.chapter)
    eq_("2", result.verse)

    result = ScriptureReference.parseRefText("Numbers 33:1")
    eq_("Numbers", result.book)
    eq_("33", result.chapter)
    eq_("1", result.verse)

    result = ScriptureReference.parseRefText("Jude 2-3")
    eq_("Jude", result.book)
    eq_(None, result.chapter)
    eq_("2-3", result.verse)

    result = ScriptureReference.parseRefText("Psalm 119:176")
    eq_("Psalm", result.book)
    eq_("119", result.chapter)
    eq_("176", result.verse)

    result = ScriptureReference.parseRefText("Psalms 1:3-4")
    eq_("Psalms", result.book)
    eq_("1", result.chapter)
    eq_("3-4", result.verse)


def test_getRefForTts():
    eq_(u'First John 1 12', ScriptureReference.
                     parseRefText("1 John 1:12").getRefForTts(Substitution()))
    eq_(u'<phoneme alphabet="ipa" ph="/ho\u028a\u02c8ze\u026a\u0259/"/> 6 13', ScriptureReference.
                     parseRefText("Hosea 6:13").getRefForTts(Substitution()))
    eq_(u'Luke 9, 1 to 2', ScriptureReference.
                     parseRefText("Luke 9:1-2").getRefForTts(Substitution()))
    eq_(u'<phoneme alphabet="ipa" ph="/s\u0251mz/"/> 1, 1 to 2',
                     ScriptureReference.parseRefText("Psalms 1:1-2").getRefForTts(Substitution()))
    eq_(u'<phoneme alphabet="ipa" ph="/s\u0251mz/"/> 1, 1 to 2',
                     ScriptureReference.parseRefText("Psalm 1:1-2").getRefForTts(Substitution()))


def test_getRefAsFile():
    eq_('1John_1_12', ScriptureReference.parseRefText("1 John 1:12").getRefAsFile())
    eq_('1John_1_12_ref.mp3', ScriptureReference.parseRefText("1 John 1:12").getRefAsFile(ext="mp3", suffix="ref"))
    eq_('1John_1_12_ver.mp3', ScriptureReference.parseRefText("1 John 1:12").getRefAsFile(ext="mp3", suffix="ver"))

    eq_("Jude_2", ScriptureReference.parseRefText("Jude 2").getRefAsFile())
    eq_("Numbers_33_1", ScriptureReference.parseRefText("Numbers 33:1").getRefAsFile())
    eq_("Jude_2-3", ScriptureReference.parseRefText("Jude 2-3").getRefAsFile())
    eq_("Psalm_119_176", ScriptureReference.parseRefText("Psalm 119:176").getRefAsFile())
    eq_("Psalms_1_3-4", ScriptureReference.parseRefText("Psalms 1:3-4").getRefAsFile())
