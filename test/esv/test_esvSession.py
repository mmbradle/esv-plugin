from nose.tools import eq_

from esv_plugin.esv.esvSession import AudioVersion
from esv_plugin.esv.esvSession import EsvAudioHTMLParser
from esv_plugin.esv.esvSession import EsvSession


def test_getMp3Link():
    esv = EsvSession()
    result = esv.getMp3Link('Heb 2:12')
    assert '' != result, 'Expected link, not blank string.'
    assert (result.find('http') > -1), 'not a url'
    assert (result.find('http://stream.esvmedia.org/mp3-play') > -1), 'unexpected url format'

    result = esv.getMp3Link('Heb 2:12', AudioVersion.HW)
    assert (result.find('/hw/') > -1), 'Expected to find hw in: %s' % result

    result = esv.getMp3Link('Heb 2:12', AudioVersion.ML)
    assert (result.find('/ml/') > -1), 'Expected to find hw in: %s' % result

    result = esv.getMp3Link('Heb 2:12', AudioVersion.MM)
    assert (result.find('/mm/') > -1), 'Expected to find hw in: %s' % result

    result = esv.getMp3Link('Heb 2:12', AudioVersion.ML_MM)
    assert (result.find('/ml/') > -1), 'Expected to find hw in: %s' % result

    result = esv.getMp3Link('Jer 1:1', AudioVersion.ML_MM)
    assert (result.find('/mm/') > -1), 'Expected to find hw in: %s' % result


def test_getFullReference():
    esv = EsvSession()
    result = esv.getFullReference('Heb 2:12')
    eq_('Hebrews 2:12', result)

    result = esv.getFullReference('Joel 2:12-13')
    eq_('Joel 2:12-13', result)


def test_getVerseContents():
    esv = EsvSession()
    result = esv.getVerseContents('Heb 2:12')
    eq_('saying, "I will tell of your name to my brothers; in the midst of the congregation I will sing your praise."', result)  # @IgnorePep8


def test_esvAudioHTMLParser():
    data = """<div class="esv">
            <h2>Hebrews 2:12
                <small class="audio">(
                    <a href="http://stream.esvmedia.org/mp3-play/hw/58002012">Listen</a>)
                </small>
            </h2>
            <div class="esv-text">
                <p id="p58002012.01-1">saying,</p>
                <div class="block-indent">
                    <p class="line-group" id="p58002012.02-1">&#8220;I will tell of your name to my brothers;
                        <br />
                        <span class="indent"></span>in the midst of the congregation I will sing your praise.&#8221;
                    </p>
                </div>
            </div>
        </div>"""

    parser = EsvAudioHTMLParser()
    result = parser.getUrl(data)
    assert '' != result, 'Expected link, not blank string.'
    assert (result.find('http') > -1), 'not a url'
    assert (result.find('http://stream.esvmedia.org/mp3-play') > -1), 'unexpected url format'
