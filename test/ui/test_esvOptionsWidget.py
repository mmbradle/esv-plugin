from nose.tools import eq_
import sys
from PyQt4 import QtGui
from PyQt4.Qt import QTest
from PyQt4.QtCore import Qt
from esv_plugin.esvOptions import EsvOptions
from esv_plugin.ui.esvOptionsWidget import EsvOptionsWidget
import mockito


class TestEsvOptionsWidget():  # pylint: disable=no-init

    def setUp(self):
        '''Create the GUI'''
        self.app = QtGui.QApplication(sys.argv)

    def test_defaults(self):
        anki_models = mockito .mock()
        mockito .when(anki_models).anki_models().thenReturn(None)

        # Setup data
        main_esv_options = EsvOptions()
        main_esv_options.useSpell = True
        main_esv_options.useIvonaTts = True
        main_esv_options.accessKey = "user@domain.com"
        main_esv_options.secretKey = "123456789abcdefg"
        form = EsvOptionsWidget(main_esv_options, anki_models)

        # Test form
        eq_(form.ui.useSpellCheckBox.isChecked(), True)
        eq_(form.ui.ttsNoneBtn.isChecked(), False)
        eq_(form.ui.ttsGoogleBtn.isChecked(), False)
        eq_(form.ui.ttsIvonaBtn.isChecked(), True)
        eq_(str(form.ui.ivonaAccessKeyText.text()), "user@domain.com")
        eq_(str(form.ui.ivonaSecretKeyText.toPlainText()), "123456789abcdefg")

        # Modify form
        QTest.mouseClick(form.ui.ttsNoneBtn, Qt.LeftButton)  # doesn't seem to be working
#         eq_(form.ui.ttsNoneBtn.isChecked(), True)
        eq_(form.ui.ttsGoogleBtn.isChecked(), False)
#         eq_(form.ui.ttsIvonaBtn.isChecked(), False)
        eq_(form.ui.ttsGroup.isVisible(), False)

        # Push OK with the left mouse button
        okWidget = form.ui.buttonBox.button(form.ui.buttonBox.Ok)
        QTest.mouseClick(okWidget, Qt.LeftButton)

        # Test model
        actualOptions = form.getEsvOptions()
#         eq_(actualOptions.useIvonaTts, False)
        eq_(actualOptions.useGoogleTts, False)
