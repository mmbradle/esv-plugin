from nose.tools import eq_

from esv_plugin.spell.spellCheck import find_substring, checkSpelling
from nose.plugins.skip import SkipTest


def test_find_substring():
    eq_(0, find_substring('a', 'abc'))


def test_tolerantCorrect():
    # Setup
    goodText = u"to put off your old self, which belongs to your former manner of life and is corrupt through deceitful desires,"  # @IgnorePep8
    userText = u"to off put your old sllf which belongs to your former manner of life and is corrupt through decietful desires"  # @IgnorePep8
    expected = u"<div style='font-size: 16.2px; font-family: Times New Roman; text-align: left;'>Mistakes: <ol margin: .2em;'><li>You missed: <span style='font-size: 18px; text-decoration: underline;'>put</span> in: <span style='font-size: 18px; text-decoration: underline;'>to put off</span></li><li>You added: <span style='font-size: 18px; text-decoration: underline;'>put</span> between: <span style='font-size: 18px; text-decoration: underline;'>off</span> and: <span style='font-size: 18px; text-decoration: underline;'>your</span></li><li style='margin-bottom: .2em;'>You wrote: <span style='font-size: 18px; text-decoration: underline;'>sllf</span><br>Replacing: <span style='font-size: 18px; text-decoration: underline;'>self,</span> in: <span style='font-size: 18px; text-decoration: underline;'>old self, which</span></li><li style='margin-bottom: .2em;'>You wrote: <span style='font-size: 18px; text-decoration: underline;'>decietful</span><br>Replacing: <span style='font-size: 18px; text-decoration: underline;'>deceitful</span> in: <span style='font-size: 18px; text-decoration: underline;'>through deceitful desires,</span></li></ol></div>" # @IgnorePep8

    # When
    actual = checkSpelling(userText, goodText)

    # Then
    eq_(expected, actual)

def test_tolerantCorrect_no_mistakes():
    # Setup
    goodText = u"no mistakes"
    userText = u"no mistakes"
    expected = "Correct!"

    # When
    actual = checkSpelling(userText, goodText)

    # Then
    eq_(expected, actual)


def test_display_in_browser():
    raise SkipTest
    goodText = u"to put off your old self, which belongs to your former manner of life and is corrupt through deceitful desires,"  # @IgnorePep8
    userText = u"to off put your old sllf blog sjoa which belongs to your former manner of life and is corrupt through decietful desires"  # @IgnorePep8
    contents = checkSpelling(userText, goodText)
    browseLocal(contents)

def strToFile(text, filename):
    """Write a file with the given name and the given text."""
    output = open(filename,"w")
    output.write(text)
    output.close()

def browseLocal(webpageText, filename='tempBrowseLocal.html'):
    '''Start your webbrowser on a local file containing the text
    with given filename.'''
    import webbrowser, os.path
    strToFile(webpageText, filename)
    webbrowser.open("file:///" + os.path.abspath(filename))