from nose.tools import eq_

from esv_plugin.spell import spellDistance


def test_getDistance():
    eq_(spellDistance.getDistance('hello', 'hllo'), 1);
    eq_(spellDistance.getDistance('hello', 'heello'), 1);
    eq_(spellDistance.getDistance('hello', 'hello'), 0);
    eq_(spellDistance.getDistance('the', 'teh'), 1);

def test_getDistanceRatio():
    eq_(spellDistance.getDistanceRatio('hello', 'hllo'), 0.8);
    eq_(spellDistance.getDistanceRatio('hello', 'hello'), 1);
    eq_(spellDistance.getDistanceRatio('hello', 'ehllo'), 0.8);