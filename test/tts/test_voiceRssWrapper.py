from nose.tools import eq_
from esv_plugin.tts.voiceRssWrapper import VoiceRssWrapper
from esv_plugin.esv.verse import ScriptureReference
from mockito import mock, when, unstub
from test.captor import Captor

save = False
useInternet = False

def test_fetch():
    response = mock()
    captor = Captor()

    import urllib2
    when(urllib2).urlopen(captor).thenReturn(response)
    when(response).read().thenReturn("okay")
    
    full_reference = "Psalm 3:3"
    text = ScriptureReference.parseRefText(full_reference).getRefForTts()
    tts = VoiceRssWrapper(text, "en-us")
    mp3 = tts.fetch()
    
    eq_(mp3, 'okay')
    eq_(captor.getValue().get_full_url(), 'http://api.voicerss.org')
    eq_(captor.getValue().get_data(), 'src=Psalm+3+3&hl=en-us&key=db0dca6538cd4640bee1544bac3a0aed')
    unstub()


def test_fetch_real():
    if useInternet:
        full_reference = "Psalm 3:3"
        text = ScriptureReference.parseRefText(full_reference).getRefForTts()
        tts = VoiceRssWrapper(text, "en-us")
        mp3 = tts.fetch()
        if save: open("test_voiceRss.mp3", "wb").write(mp3)
        assert (1000 <= len(mp3) <= 20000), "len(mp3): {} is not within reasonable size range.".format(len(mp3))
