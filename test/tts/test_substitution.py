from nose.tools import eq_
from esv_plugin.tts.substitution import Substitution

def test_Substitute():
    substitution = Substitution()
    eq_(substitution.getPronounciation("Ruth"), "Ruth")
