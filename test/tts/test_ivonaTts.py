from esv_plugin.tts import ivonaTts
from mockito import mock, when, unstub
from test.captor import Captor
from mockito import mock, when, unstub
from test.captor import Captor
from nose.tools import eq_

save = False
useInternet = False

def test_fetch():
    response = mock()
    captor = Captor()

    import urllib2
    when(urllib2).urlopen(captor).thenReturn(response)
    when(response).read().thenReturn("okay")
    
    tts = ivonaTts.create_voice(access_key='GDNAIIEEWI3UAJ56JMGA', secret_key='2wwMGW0P8+HHhkcoAEV9byFOBV+XBIkzNj6r1BQY')
    mp3 = tts.fetch("My name is Emma and I am an ivona voice")
    
    eq_(mp3, 'okay')
    eq_(captor.getValue().get_full_url(), 'https://tts.us-east-1.ivonacloud.com/CreateSpeech')
    eq_(captor.getValue().get_data(), '{"Input": {"Type": "application/ssml+xml", "Data": "My name is Emma and I am an ivona voice"}, "OutputFormat": {"Codec": "MP3"}, "Voice": {"Name": "Emma"}, "Parameters": {"ParagraphBreak": 650, "Rate": "medium", "SentenceBreak": 400}}')
    unstub()


def test_fetch_real():
    if useInternet:
        tts = ivonaTts.create_voice(access_key='GDNAIIEEWI3UAJ56JMGA', secret_key='2wwMGW0P8+HHhkcoAEV9byFOBV+XBIkzNj6r1BQY')
        mp3 = tts.fetch("My name is Emma and I am an ivona voice")
        assert (10000 <= len(mp3) <= 20000), "len(mp3): {} is not within reasonable size range.".format(len(mp3))
 
        if save: 
            open("test.mp3", "wb").write( mp3 )


def test_textToSpeech_Bug1():
    if useInternet:
        tts = ivonaTts.create_voice(access_key='GDNAIIEEWI3UAJ56JMGA', secret_key='2wwMGW0P8+HHhkcoAEV9byFOBV+XBIkzNj6r1BQY')
        mp3 = tts.fetch("Ezekiel 33 6")
        open("test_Bug1.mp3", "wb").write(mp3)
        assert (10000 <= len(mp3) <= 20000), "len(mp3): {} is not within reasonable size range.".format(len(mp3))
